import pandas as pd
import math


depenses = pd.read_csv("depenses.csv")

print(depenses)
    
moyennes = depenses.mean()
variances = depenses.var()

for i in range (depenses.shape[0]) :
    #mise en place des id
    depenses.iloc[i,0] = "id" + str(i)
    depenses.iloc[i,1] = "ville" + str(i)
    #centrage et réduction des colonnes numériques
    depenses.iloc[i,2] = (depenses.iloc[i,2] - moyennes[0])/math.sqrt(variances[0])
    depenses.iloc[i,3] = (depenses.iloc[i,3] - moyennes[1])/math.sqrt(variances[1])
    depenses.iloc[i,4] = (depenses.iloc[i,4] - moyennes[2])/math.sqrt(variances[2])

print(depenses) 

depenses.to_csv('depenses_anonymes.csv')